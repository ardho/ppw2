from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest, JsonResponse
from django.core.exceptions import ValidationError
from django.contrib.staticfiles.testing import StaticLiveServerTestCase

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

import time, json

from . import views
from .models import User
from .forms import RegUserForm

# Create your tests here.
class Story10UnitTest(TestCase):

    def test_index_page_using_index_func(self):
        found = resolve('/s10/')
        self.assertEqual(found.func, views.index)

    def test_index_page_url_is_exist(self):
        response = Client().get('/s10/')
        self.assertEqual(response.status_code, 200)

    def test_isEmailValid_page_using_isEmailValid_func(self):
        found = resolve('/s10/isEmailValid')
        self.assertEqual(found.func, views.isEmailValid)

    def test_isEmailValid_page_url_is_exist(self):
        response = Client().get('/s10/isEmailValid')
        self.assertEqual(response.status_code, 200)

    def test_add_subscriber_page_using_add_subscriber_func(self):
        found = resolve('/s10/add_user_as_subscriber')
        self.assertEqual(found.func, views.add_user_as_subscriber)

    def test_forbidden_add_subscriber_with_no_data(self):
        response = Client().get('/s10/add_user_as_subscriber')
        self.assertEqual(response.status_code, 403)

class SeleniumSubscribeTest(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path = "./chromedriver.log"
        service_args = ['--verbose']
        cls.browser  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        cls.browser.implicitly_wait(25)
        cls.browser.get('%s%s' % (cls.live_server_url, '/s10/'))

    @classmethod
    def tearDownClass(cls):
        cls.browser.quit()
        super().tearDownClass()

    def test1_subscribe(self):
        name = self.browser.find_element_by_id('id_name')
        email = self.browser.find_element_by_id('id_email')
        password = self.browser.find_element_by_id('id_password')
        button = self.browser.find_element_by_id('submit_button')

        name.send_keys('lalaaa')
        email.send_keys('coba@email.com')
        password.send_keys('zzzzzzzz')
        time.sleep(5)
        response = Client().get('/s10/add_user_as_subscriber')
        self.assertEqual(response.status_code, 403)

        # button.send_keys(Keys.RETURN)
        # time.sleep(5)

        # infoFailed = self.browser.find_element_by_class_name("alert-danger").value_of_css_property("display")
        # self.assertEqual(infoFailed, "none")
        # infoSuccess = self.browser.find_element_by_class_name("alert-success").value_of_css_property("display")
        # self.assertEqual(infoSuccess, "block") 
        # time.sleep(5)

