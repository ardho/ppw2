from django.test import TestCase, Client
from django.urls import resolve
from . import views

# Create your tests here.
class Story9UnitTest(TestCase):

	def test_story_9_url_is_exist(self):
		response = Client().get('/s9/')
		self.assertEqual(response.status_code, 200)

	def test_getJson_page_url_is_exist(self):
		response = Client().get('/s9/getJSON/')
		self.assertEqual(response.status_code, 200)    

	def test_books_page_using_index_func(self):
		found = resolve('/s9/')
		self.assertEqual(found.func, views.index)

	def test_getJson_using_getJson_func(self):
		found = resolve('/s9/getJSON/')
		self.assertEqual(found.func, views.getJSON)

	def test_using_profile_template(self):
		response = Client().get('/s9/')
		self.assertTemplateUsed(response, 'index-s9.html')