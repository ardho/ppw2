from django.urls import path
from . import views

app_name = 's8'

urlpatterns = [
    path('', views.index),
]