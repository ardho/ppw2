$(document).ready(function(){
    var flag = true;
    $('.slider.round').click(function(){
        if(flag){
        	// black theme
            $('body').css('background-color', '#424242');
            $('.desc').css('color', '#FAFAFA');
            flag = false;
        }
        else{
        	// white theme
            $('body').css('background-color', '#FAFAFA');
            $('.desc').css('color', '#424242')
            flag = true;
        }
    });
 	$('#accordion .accordion-toggle').addClass('collapsed'); 
	$('#accordion').find('.accordion-toggle').click(function(){
	 	//Expand or collapse this panel
	  	$(this).next().slideToggle('slow');

	  	//toggles between collapsed and expanded classes
		$(this).toggleClass('collapsed expanded');
	  
	  	//removes any instance of "expanded" from the other parents and replace it with the "collapsed" class
	    $("#accordion .accordion-toggle").not($(this)).removeClass('expanded').addClass('collapsed');
	  
	  	//Hide the other panels
	  	$(".accordion-content").not($(this).next()).slideUp('slow');  
	});
});

// Challenge 8
$(function() {
    $(".se-pre-con").fadeOut(3000, function() {
        $(".content").fadeIn(3000);        
    });
});
